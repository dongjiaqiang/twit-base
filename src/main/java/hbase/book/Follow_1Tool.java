package hbase.book;

import hbase.book.model.Follow_1;
import hbase.book.model.Follow_1Dao;

import java.io.IOException;
import java.util.Arrays;
import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.hbase.client.HConnection;
import org.apache.hadoop.hbase.client.HConnectionManager;

public class Follow_1Tool {

	/**
	 * @param args
	 * @throws IOException 
	 */
	
	//我们思考HBase Schema时,应该关注以下几点
	public static void main(String[] args) throws IOException {
		// TODO Auto-generated method stub
		Configuration conf=new Configuration();		
		HConnection connection=HConnectionManager.createConnection(conf);
		Follow_1Dao dao=new Follow_1Dao(connection);
		
		//某用户关注的谁---参数是某用户---返回值是所关注用户的列表
		if("get".equals(args[0])){
			Follow_1 f=dao.getFollows(args[1]);
			System.out.println(f);
			return;
		}
		
		//一个用户关注了某用户---参数是某用户和被关注用户列表
		if("put".equals(args[0])){
			dao.putFollows(new Follow_1(args[1],Arrays.asList(args[2].split(","))));
			return;
		}
			
		//一个用户是否关注了另一个用户---参数是某用户和另一个用户
		if("isfollow".equals(args[0])){
			 System.out.println(dao.isFollow(args[1], args[2])?"follow":"no follow");
			 return;
		}
		
		//一个用户取消关注用户---参数是某用户和被取消关注的用户列表
		if("cancel".equals(args[0])){
			System.out.println("cancel "+dao.deleteFollow(args[1],args[2].split(","))+" people");
			return;
		}
	}

	
}
