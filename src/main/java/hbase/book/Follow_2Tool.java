package hbase.book;

import hbase.book.model.Follow_2;
import hbase.book.model.Follow_2Dao;

import java.io.IOException;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.hbase.client.HConnection;
import org.apache.hadoop.hbase.client.HConnectionManager;

public class Follow_2Tool {

	/*
	 * HBase提供了数据管理的一种新方式,在设计HBase表时需要考虑的因素,和决定是否使用关系型系统时需要作出的权衡.
	 * 
	 * 模式设计的出发点是问题.而不是关系.在HBase做设计时,需要考虑是如何为一个问题有效地寻找答案.而不是实体模型是否纯正.
	 * 
	 */
	/**
	 * @param args
	 * @throws IOException 
	 */
	public static void main(String[] args) throws IOException {
		// TODO Auto-generated method stub

		Configuration conf=new Configuration();		
		HConnection connection=HConnectionManager.createConnection(conf);
		Follow_2Dao dao=new Follow_2Dao(connection);
		
		//某用户关注的谁---参数是某用户---返回值是所关注用户的列表
		if("get".equals(args[0])){
			Follow_2 f=dao.getFollows(args[1]);
			System.out.println(f);
			return;
		}
		
		//一个用户关注了某用户---参数是某用户和被关注用户列表
		if("put".equals(args[0])){
			dao.putFollow(args[1],args[2].split(","));
			return;
		}
			
		//一个用户是否关注了另一个用户---参数是某用户和另一个用户
		if("isfollow".equals(args[0])){
			 System.out.println(dao.isFollow(args[1], args[2])?"follow":"no follow");
			 return;
		}
		
		//一个用户取消关注用户---参数是某用户和被取消关注的用户列表
		if("cancel".equals(args[0])){
			dao.deleteFollow(args[1], args[2].split(","));
			System.out.println("Deleted complete...");
			return;
		}
	}

}
