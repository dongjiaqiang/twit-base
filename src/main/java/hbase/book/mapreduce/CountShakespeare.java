package hbase.book.mapreduce;

import hbase.book.model.TwitDao;

import java.io.IOException;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.hbase.HBaseConfiguration;
import org.apache.hadoop.hbase.client.Result;
import org.apache.hadoop.hbase.client.Scan;
import org.apache.hadoop.hbase.io.ImmutableBytesWritable;
import org.apache.hadoop.hbase.mapreduce.TableMapReduceUtil;
import org.apache.hadoop.hbase.mapreduce.TableMapper;
import org.apache.hadoop.hbase.util.Bytes;
import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Job;
import org.apache.hadoop.mapreduce.lib.output.NullOutputFormat;


//分布式的HBase,HDFS和MapReduce

//Hadoop为HBase提供分布式计算框架,支持高吞吐量数据访问
//HDFS作为HBase的存储层,支持高可用和可靠性

//分布式模式的HBase
//1.切分和分配大表

//在HBase中,数据也是存放于表中,而这些表的大小可能是数十亿行和数十亿列,这就将导致一个表不可能存储于一台服务器中.表将被切分为小一点的数据单位Region.托管region的服务器叫做regionServer.
//每个regionServer托管多个region

//2.如何找到region

//3.HBase和Map Reduce结合
//HBase可以作为数据源,HBase可以作为接受数据,HBase可以共享数据


public class CountShakespeare {

	public static class Map extends TableMapper<Text,LongWritable>{
		public static enum Counters{ROWS,SHAKESPEARE}
		
		private boolean containShakespeare(String msg){
			if(msg.contains("Hamlett")||msg.contains("Othello"))
				return true;
			return false;
		}

		//MapReduce mapper作业从HBase中取得region作为输入源,每个region默认启动一个mapper任务
		//ImmutableBytesWritable key这个值表示行键的值
		//Result value这个值是scan实例扫描的结果值
		
		@Override
		protected void map(ImmutableBytesWritable key, Result value,
				Context context) throws IOException, InterruptedException {
			// TODO Auto-generated method stub
			byte[] b=value.getValue(TwitDao.TWIT_FAM, TwitDao.TWIT_COL);
			String msg=Bytes.toString(b);
			if(msg!=null&&!msg.isEmpty())
				context.getCounter(Counters.ROWS).increment(1);
			if(containShakespeare(msg))
				context.getCounter(Counters.SHAKESPEARE).increment(1);
			
		}				
	}
	/**
	 * @param args
	 * @throws IOException 
	 * @throws ClassNotFoundException 
	 * @throws InterruptedException 
	 */
	public static void main(String[] args) throws IOException, InterruptedException, ClassNotFoundException {
		// TODO Auto-generated method stub
		Configuration conf=HBaseConfiguration.create();		
		Job job=Job.getInstance(conf, "TwitBase Shakespeare counter");
		job.setJarByClass(CountShakespeare.class);
		Scan scan=new Scan();
		scan.addColumn(TwitDao.TWIT_FAM, TwitDao.TWIT_COL);
		TableMapReduceUtil.initTableMapperJob(Bytes.toString(TwitDao.TABLE_NAME), scan, Map.class, ImmutableBytesWritable.class, Result.class, job);
		
		job.setOutputFormatClass(NullOutputFormat.class);
		job.setNumReduceTasks(0);
		System.out.println(job.waitForCompletion(true)?0:1);
		
	}

}
