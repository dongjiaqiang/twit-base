package hbase.book;

import hbase.book.model.User;
import hbase.book.model.UserDao;

import java.io.IOException;


import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.hbase.client.HConnection;
import org.apache.hadoop.hbase.client.HConnectionManager;

public class UsersTool {

	/**
	 * @param args
	 * @throws IOException 
	 */
	public static final String usage="UsersTool action ...\n"+
									 " help - print this message and exit.\n"+
			                         " add user name email passwd"+"- add a new user.\n"+
									 " get user - retrieve a specific user .\n"+
			                         " list - list all installed users.\n";		
	//无模式数据库(创建表的时候只需要指定表名字和列族)---有时间戳版本数据库(每个单位都可以设定要保存的时间戳版本数量)---面向列族的数据库(列族直接关系到物理模型)---键值对数据库(四维键值对映射)
	
	//一.创建表---create 'user','info'---创建表时必须指定表名字和列族名字---没有指定具体的列和数据类型---所以可以称为无模式数据类型					
	//强制指定表名字和列族名字
	
	//二.HBase的Java API	
	//1.获取HBase的连接
	
	//这只是与一个HBase数据表进行连接---直接创建一个表句柄---开销大
	//HTableInterface userTable=new HTable(HBaseConfiguration.create(), "user");
	//而这里相当于创建一个连接池---由这个池分配连接---开销小
	//HConnection connection=HConnectionManager.createConnection(conf);
	
	//2.操作HBase---HBase的表由一个行键唯一标识---所以行键第一重要---任何操作实例都是先基于行键
	//这些实例都先由行键构造而成,而后依据其他限定来修改
	
	//读HBase---Get实例		
	//写或修改HBase数据---Put实例
	//删除HBase数据---Delete实例
	//扫描HBase数据---Scan实例
	//递增HBase数据---Increment实例	
	
	//最后实例通过句柄进行提交
	
	//3.关闭与HBase连接---connection.close()
	
	//三.HBase的写机制
	
	//HBase在写入时都会写入两个地方,预写式日志和MemStore,只有这两个地方都写入正确,才认为数据被真正写入
	
	//MemStore是内存中的写入缓冲区---HBase中数据在永久写入硬盘时先在这里被积累.当MemStore被填满时,其中的数据就会被刷写到硬盘中.生成一个HFile.HFile是HBase使用的底层数据结构
	//HFile对应与列族,一个列族可以有多个HFile,但是一个HFile不能由多个列族.在集群中的每个节点,每个列族都有一个MemStore.
	
	//大型分布式系统的硬件故障很常见---所以会有WAL来保证写入成功
	
	//1.HBase在写入数据时,要写入两个地方,分别是WAL和MemStore,只有这两个地方成功写入,才能保证数据被成功写入HBase
	//2.MemStore---是一个内存写入缓冲区---对应于列族---每个节点中的每个节点都有一个MemStore
	//3.当往HBase写入数据时---先往MemStore中写,当MemStore写满的时候---就会溢写到硬盘中---写入一个称为HFile的底层文件---对应HFile,是每个HFile中只能存在一个列族的数据
	//4.WAL在每个节点中都存在,这个节点所有的表和列族共享WAL---如果禁止使用HFile---当某个节点挂掉时,可能导致该节点中的MemStore的数据丢失

	//四.HBase的读机制
	
	//如果你想快速访问数据,通用的原则是保持数据有序并尽可能保存于内存中.HBase读操作必须重新衔接持久化到硬盘中的HFile和内存中的MemStore中的数据.
	//HBase在读操作中使用LRU缓存技术.这种缓存技术叫做BlockCache,和MemStore在一个JVM堆中,BlockCache设计用来保存从HFile里读入内存的频繁访问的数据,避免硬盘读.每个列族都有BlockCache
	
	//HFile存放某时刻MemStore刷写时的快照.一个完整行的数据可能存放在多个HFile中.为了读出完整的行,HBase需要读取包含该信息的所有HFile.
	
	
	//五.HBase的合并机制
	
	
	
	
	//六.HBase的数据模型概括
	
	//1.逻辑实体---这六大概念是HBase的基础,这是对物理存放在硬盘中的数据进行管理的基石---要牢记!!!!!
	
	//表(Table)---HBase用表来组织数据,表名字是字符串---HBase最顶层结构---字符串
	
	//行(Row)---在表中,数据按行存储.行由行键唯一标识.行键没用数据类型,总是视为字节数组---行由行键唯一标识,即每行数据其行键是唯一的---字节数组
	
	//列族(Column Family)---行中的数据按照列族分组,列族也影响着HBase数据的物理存放.表中每行拥有相同列族.列族名字是字符串.---列族影响表的物理模型---字符串
	
	//列限定符(Column qualifier)---列族中的数据通过列限定符或列来定位.列限定符不必事前定义,不必在不同行间保持一致.---字节数组
	
	//单元(Cell)---行键,列族和列限定符一起确定一个单元,存储在单元中的数据就是值,无数据类型---字节数组
	
	//时间版本(Version)---单元值具有时间版本,时间版本由时间戳标识,是一个long值.---可以设定保留的时间戳版本个数
	
	//HBase的每个数据值使用坐标来访问.一个值的完整坐标包括行键,列族,列限定符和时间版本---确定cell
	
	
	
	//七.数据坐标---对应到SQL数据库中在表中定义一个值采用2维坐标,而在HBase中定位一个值采用4维坐标---所以HBase可以看做是一个KV数据库
	
	//当使用HBase API检索数据时,不必提供所有坐标---单元坐标越少,对应值的集合范围越广
	
	//时间版本->列限定符->列族->行键
	
	//九.数据模型---HBase设计上没有严格的形态数据---数据记录可能包含不一致的列,不确定的大小---都是半结构化数据
	
	//逻辑模型---有序映射的映射---这些键值是有序的
	
	//物理模型---面向列族的
	//HBase中的表由行和列组成.HBase中列按照列族分组.每个列族在硬盘中有自己的HFile集合.这种物理上的隔离允许在列族底层HFile层面上分别进行管理.	
	//HFile中每条记录都是完整的.列族都是物理存放在一起的

	public static void main(String[] args) throws IOException {
		// TODO Auto-generated method stub

		if(args.length==0||"help".equals(args[0])){
			System.out.println(usage);
			System.exit(0);
		}
		
		
		Configuration conf=new Configuration();
		
		//使用连接池来管理与HBase的连接		
		HConnection connection=HConnectionManager.createConnection(conf);
		
		
		UserDao dao=new UserDao(connection);
		
		if("get".equals(args[0])){
			System.out.println("Getting user "+args[1]);
			User u=dao.getUser(args[1]);
			System.out.println(u);
		}
		
		if("add".equals(args[0])){
			System.out.println("Adding user...");
			dao.addUser(args[1], args[2], args[3], args[4]);
			User u=dao.getUser(args[1]);
			System.out.println("Successfully added user "+u);
		}
		
		if("list".equals(args[0])){
			for(User u:dao.getUsers())
				System.out.println(u);
		}
		connection.close();		
		//小结
		
		//HBase是专门为半结构化数据和水平可扩展设计的数据库.它把数据存储在表中.在表中,数据按照一个四维坐标系统来组织:行键,列族,列限定符和时间版本.
		//HBase也是一个无模式数据库,只需提前定义列族.它也是无类型的数据库.所有数据都是字节数组存储在数据库中.
		//只需提前定义列族,Get,Put,Delete,Increment和Scan.
		
		//HBase操作是原子不可分的---Put操作是整体成功或失败
		//行间操作不是原子性的
		//对于给定行的多个写操作,总是以每个写操作作为整体彼此独立的.
		//对于给定Get操作,返回系统当时保存的完整行
		//
		
	}

}
