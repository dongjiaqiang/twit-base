package hbase.book;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.hbase.HBaseConfiguration;
import org.apache.hadoop.hbase.HColumnDescriptor;
import org.apache.hadoop.hbase.HTableDescriptor;
import org.apache.hadoop.hbase.MasterNotRunningException;
import org.apache.hadoop.hbase.TableNotFoundException;
import org.apache.hadoop.hbase.ZooKeeperConnectionException;
import org.apache.hadoop.hbase.client.HBaseAdmin;
import org.apache.hadoop.hbase.util.Bytes;
public class CommonsTool {

	public static String usage="CommonsTool action..."+
			 				   " help - print this message and exit.\n"+
                               " create table "+"- create a new table.\n"+
			 				   " drop table "+"- drop a table.\n"+
                               " list column family of table.\n";
	/**
	 * @param args
	 * @throws IOException 
	 * @throws ZooKeeperConnectionException 
	 * @throws MasterNotRunningException 
	 */
	public static void main(String[] args) throws MasterNotRunningException, ZooKeeperConnectionException, IOException {
		// TODO Auto-generated method stub
		if(args.length==0||"help".equals(args[0])){
			System.out.println(usage);
			System.exit(0);
		}
		
		if("create".equals(args[0])){
			System.out.println("Create table "+args[1]);
			createTable(args[1], args[2].split(","),1,HBaseConfiguration.create());
		}
		
		if("drop".equals(args[0])){
			System.out.println("Drop table "+args[1]);
			dropTable(args[1], HBaseConfiguration.create());
		}
		
		if("list".equals(args[0])){
			System.out.println("column Families");
			for(String name:getColumnFamilies(args[1],HBaseConfiguration.create()))
				System.out.println(name);
		}
	}
	
	//创建HBase表,传入表名字,表的列族,表列族限定的时间戳版本
	public static void createTable(String tableName,String[] columnFamilys,int versionNum,Configuration conf) throws MasterNotRunningException, ZooKeeperConnectionException, IOException{
		HBaseAdmin admin=new HBaseAdmin(conf);
		@SuppressWarnings("deprecation")
		HTableDescriptor desc=new HTableDescriptor(tableName);
		for(String columnFamily:columnFamilys)
			desc.addFamily(new HColumnDescriptor(columnFamily.trim()).setMaxVersions(versionNum));
		   admin.createTable(desc);		
	}
	
	public static void dropTable(String tableName,Configuration conf) throws IOException{
		HBaseAdmin admin=new HBaseAdmin(conf);		
		admin.disableTable(tableName);
		admin.deleteTable(tableName);
	}
	
	public static List<String> getColumnFamilies(String tableName,Configuration conf) throws TableNotFoundException, IOException{
		HBaseAdmin admin=new HBaseAdmin(conf);	
		HTableDescriptor desc=admin.getTableDescriptor(Bytes.toBytes(tableName));
		List<String> names=new ArrayList<String>();
		for(HColumnDescriptor cd:desc.getColumnFamilies())
			names.add(cd.getNameAsString());
		return names;
	}

}
