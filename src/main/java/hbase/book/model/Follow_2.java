package hbase.book.model;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.apache.hadoop.hbase.KeyValue;
import org.apache.hadoop.hbase.client.Result;
import org.apache.hadoop.hbase.util.Bytes;

public class Follow_2 {

	public String user;
	public List<String> follows=new ArrayList<String>();
	
	public Follow_2(String user, List<String> follows) {
		super();
		this.user = user;
		this.follows = follows;
	}
	
	@SuppressWarnings("deprecation")
	public Follow_2(Result r){
		this.user=Bytes.toString(r.getRow());
		List<KeyValue> kvs=r.list();
		for(KeyValue kv:kvs)
			follows.add(Bytes.toString(kv.getQualifier()));
	}

	@Override
	public String toString() {
		// TODO Auto-generated method stub
		return new String("<User: "+user+" "+StringUtils.join(follows, ",")+" >");
	}
	
	
}
