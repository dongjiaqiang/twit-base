package hbase.book.model;

import java.io.IOException;
import java.util.List;

import org.apache.hadoop.hbase.KeyValue;
import org.apache.hadoop.hbase.client.Delete;
import org.apache.hadoop.hbase.client.Get;
import org.apache.hadoop.hbase.client.HConnection;
import org.apache.hadoop.hbase.client.HTableInterface;
import org.apache.hadoop.hbase.client.Put;
import org.apache.hadoop.hbase.client.Result;
import org.apache.hadoop.hbase.util.Bytes;

public class Follow_1Dao {

	public static final byte[] TABLE_NAME=Bytes.toBytes("follows_1");	
	public static final byte[] FOLLOW_FAM=Bytes.toBytes("follows");
	public static final byte[] COUNT_COL=Bytes.toBytes("count");
	public static final byte[] INDEX_COL=Bytes.toBytes("index");
			
	private HConnection connection;	
	public Follow_1Dao(HConnection connection){
		this.connection=connection;
	}
	
	
	private static Get mkGet(String user){
		Get g=new Get(Bytes.toBytes(user));
		g.addFamily(FOLLOW_FAM);
		return g;
	}
	
	
	private static Put mkPut(Follow_1 f,long index,long count){
		Put p=new Put(Bytes.toBytes(f.user));
		for(int i=0;i<f.follows.size();i++)
			p.add(FOLLOW_FAM,Bytes.toBytes(i+index),Bytes.toBytes(f.follows.get(i).trim()));	
		p.add(FOLLOW_FAM, COUNT_COL,Bytes.toBytes(count+f.follows.size()));
		p.add(FOLLOW_FAM, INDEX_COL, Bytes.toBytes(index+f.follows.size()));
		return p;
	}
	 
	private static Delete mkDelete(String user){
		Delete delete=new Delete(Bytes.toBytes(user));
		return delete;
	}
	
	public Follow_1 getFollows(String user) throws IOException{
		Get g=mkGet(user);
		HTableInterface follows=connection.getTable(TABLE_NAME);
		Result r=follows.get(g);
		follows.close();
		return new Follow_1(r);
	}
	
	public void putFollows(Follow_1 f) throws IOException{
		
		HTableInterface follows=connection.getTable(TABLE_NAME);
		Get g=mkGet(f.user);
		Result r=follows.get(g);
		
		byte[] indexBytes=r.getValue(FOLLOW_FAM, INDEX_COL);
		long index=indexBytes==null?0:Bytes.toLong(indexBytes);
		byte[] countBytes=r.getValue(FOLLOW_FAM, COUNT_COL);
		long count=countBytes==null?0:Bytes.toLong(countBytes);
		
		Put p=mkPut(f,index,count);
		
		follows.put(p);
		/*follows.incrementColumnValue(Bytes.toBytes(f.user), FOLLOW_FAM, COUNT_COL, f.follows.size());*/
		follows.close();
	}
	
	
	public boolean isFollow(String user,String followed) throws IOException{
		Follow_1 f=getFollows(user);
		for(String follow:f.follows)
			if(follow.equals(followed))
				return  true;
		return false;
	}
	
	@SuppressWarnings("deprecation")
	public long deleteFollow(String user,String[] followeds) throws IOException{
		Delete d=mkDelete(user);
		long num=0;
		Get g=mkGet(user);
		HTableInterface follows=connection.getTable(TABLE_NAME);
		Result r=follows.get(g);
		List<KeyValue> kvs=r.list();
		for(KeyValue kv:kvs)
			for(String followed:followeds)
				if(Bytes.toString(kv.getValue()).equals(followed)){				
					d.deleteColumns(FOLLOW_FAM, kv.getQualifier());
					num++;
				}
		if(num!=0)
			follows.delete(d);
		follows.incrementColumnValue(Bytes.toBytes(user), FOLLOW_FAM, COUNT_COL, num*-1);
		follows.close();
		return num;
	}
	
}
