package hbase.book.model;

import java.io.IOException;

import org.apache.hadoop.hbase.client.Delete;
import org.apache.hadoop.hbase.client.Get;
import org.apache.hadoop.hbase.client.HConnection;
import org.apache.hadoop.hbase.client.HTableInterface;
import org.apache.hadoop.hbase.client.Put;
import org.apache.hadoop.hbase.client.Result;
import org.apache.hadoop.hbase.util.Bytes;

public class Follow_2Dao {

	public static final byte[] TABLE_NAME=Bytes.toBytes("follows_2");	
	public static final byte[] FOLLOW_FAM=Bytes.toBytes("follows");
		
	private HConnection connection;	
	public Follow_2Dao(HConnection connection){
		this.connection=connection;
	}
	
	private static Put mkPut(String user,String[] follows){
		Put p=new Put(Bytes.toBytes(user));
		for(String follow:follows)
			p.add(FOLLOW_FAM, Bytes.toBytes(follow), Bytes.toBytes(1));
		return p;
	}
	
	private static Get mkGet(String user){
		Get g=new Get(Bytes.toBytes(user));
		g.addFamily(FOLLOW_FAM);		
		return g;
	}
	
	private static Get mkGet(String user,String follow){
		Get g=mkGet(user);
		g.addColumn(FOLLOW_FAM, Bytes.toBytes(follow));
		return g;
	}
	
	private static Delete mkDelete(String user,String[] follows){
		Delete d=new Delete(Bytes.toBytes(user));
		for(String follow:follows)
			d.deleteColumns(FOLLOW_FAM, Bytes.toBytes(follow));
		return d;
	}
	
	public Follow_2 getFollows(String user) throws IOException{
		HTableInterface follows=connection.getTable(TABLE_NAME);
		Get g=mkGet(user);
		Result r=follows.get(g);
		follows.close();
		return new Follow_2(r);
	}
	
	public boolean isFollow(String follow,String followed) throws IOException{
		HTableInterface follows=connection.getTable(TABLE_NAME);
		Get g=mkGet(follow,followed);
		Result r=follows.get(g);
		follows.close();
		return r.getValue(FOLLOW_FAM, Bytes.toBytes(followed))==null?false:true;
	}
	
	public void putFollow(String user,String[] follows) throws IOException{
		HTableInterface f=connection.getTable(TABLE_NAME);
		Put p=mkPut(user,follows);
		f.put(p);
		f.close();
	}
	
	public void deleteFollow(String user,String[] follows) throws IOException{
		HTableInterface f=connection.getTable(TABLE_NAME);
		Delete d=mkDelete(user, follows);
		f.delete(d);
		f.close();
	}
	
}
