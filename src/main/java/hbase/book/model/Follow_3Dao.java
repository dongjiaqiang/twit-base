package hbase.book.model;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import hbase.book.utils.MD5Utils;

import org.apache.hadoop.hbase.client.Delete;
import org.apache.hadoop.hbase.client.Get;
import org.apache.hadoop.hbase.client.HConnection;
import org.apache.hadoop.hbase.client.HTableInterface;
import org.apache.hadoop.hbase.client.Put;
import org.apache.hadoop.hbase.client.Result;
import org.apache.hadoop.hbase.client.ResultScanner;
import org.apache.hadoop.hbase.client.Scan;
import org.apache.hadoop.hbase.util.Bytes;

public class Follow_3Dao {

	public static final byte[] TABLE_NAME=Bytes.toBytes("follows_3");	
	public static final byte[] FOLLOW_FAM=Bytes.toBytes("follows");
	
	private HConnection connection;	
	public Follow_3Dao(HConnection connection){
		this.connection=connection;
	}	
	
	private static byte[] getRow(String user,String follow){
		byte[] userBytes=MD5Utils.md5sum(user);
		byte[] row=new byte[MD5Utils.MD5_LENGTH*2];				
		Bytes.putBytes(row, 0, userBytes, 0, MD5Utils.MD5_LENGTH);
		Bytes.putBytes(row,MD5Utils.MD5_LENGTH,Bytes.toBytes(follow),0,MD5Utils.MD5_LENGTH);	
		return row;
	}
	private static Put mkPut(String user,String follow,String followName){
			
		Put p=new Put(getRow(user, follow));
		p.add(FOLLOW_FAM, Bytes.toBytes(follow), Bytes.toBytes(followName));		
		return p;
	}
	
	
	private static List<Put> mkPut(String user,String[] follows,String[] followNames){
		List<Put> puts=new ArrayList<Put>();
		for(int i=0;i<follows.length;i++)
			puts.add(mkPut(user, follows[i], followNames[i]));
		return puts;
	}
	
	private String[] getUserName(String[] users) throws IOException{
		HTableInterface f=connection.getTable(UserDao.TABLE_NAME);
		List<Get> gets=new ArrayList<Get>();
		for(String user:users)
			gets.add(new Get(Bytes.toBytes(user)));
		Result[] rs=f.get(gets);
		f.close();
		String[] names=new String[users.length];
		for(int i=0;i<names.length;i++)
			names[i]=Bytes.toString(rs[i].getValue(UserDao.INFP_FAM, UserDao.NAME_COL));
		return names;
	}
	
	private static Delete mkDelete(String user,String follow){		
		return new Delete(getRow(user, follow));
	}
	
	private static Scan mkScan(String user){
		Scan scan=new Scan();
		byte[] start=MD5Utils.md5sum(user);
		byte[] stop=MD5Utils.md5sum(user);
		stop[stop.length-1]++;
		scan.setStartRow(start);
		scan.setStopRow(stop);
		return scan;		
	}
	
	private static Get mkGet(String user,String follow){
		return new Get(getRow(user, follow));
	}
	
	@SuppressWarnings("deprecation")
	public Follow_2 getFollows(String user) throws IOException{
		HTableInterface f=connection.getTable(TABLE_NAME);
		Scan s=mkScan(user);
		ResultScanner rs=f.getScanner(s);
		List<String> follows=new ArrayList<String>();
		for(Result r:rs)
			follows.add(Bytes.toString(r.list().get(0).getQualifier()));
		return new Follow_2(user, follows);		
	}
	
	public void deleteFollow(String user,String follow) throws IOException{
		HTableInterface f=connection.getTable(TABLE_NAME);			
		f.delete(mkDelete(user,follow));
	}
	
	public boolean isFollow(String user,String follow) throws IOException{
		HTableInterface f=connection.getTable(TABLE_NAME);
		Result r=f.get(mkGet(user, follow));
		return !r.isEmpty();
	}
	
	public void putFollows(String user,String[] follows) throws IOException{
		HTableInterface f=connection.getTable(TABLE_NAME);
		String[] names=getUserName(follows);
		f.put(mkPut(user, follows, names));
		f.close();
	}
	
	
}
