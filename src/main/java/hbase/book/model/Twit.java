package hbase.book.model;

import hbase.book.utils.MD5Utils;

import org.apache.hadoop.hbase.client.Result;
import org.apache.hadoop.hbase.util.Bytes;
import org.joda.time.DateTime;

public class Twit {

	public String user;
	public DateTime time;
	public String twit;
	
	public Twit(String user, DateTime time, String twit) {
		super();
		this.user = user;
		this.time = time;
		this.twit = twit;
	}

	@Override
	public String toString() {
		// TODO Auto-generated method stub
		return String.format("<Twit:%s, %s, %s>", this.user,this.time,this.twit);
	}
	
	public Twit(Result r){
		this.user=Bytes.toString(r.getValue(TwitDao.TWIT_FAM, TwitDao.USER_COL));
		this.twit=Bytes.toString(r.getValue(TwitDao.TWIT_FAM, TwitDao.TWIT_COL));
		byte[] timeStamp=new byte[Long.SIZE/8];
		timeStamp=Bytes.copy(r.getRow(), MD5Utils.MD5_LENGTH,r.getRow().length-MD5Utils.MD5_LENGTH);
		this.time=new DateTime(Bytes.toLong(timeStamp)*-1);
	}		
}
