package hbase.book.model;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;


import org.apache.hadoop.hbase.client.Delete;
import org.apache.hadoop.hbase.client.Get;
import org.apache.hadoop.hbase.client.HConnection;
import org.apache.hadoop.hbase.client.HTableInterface;
import org.apache.hadoop.hbase.client.Put;
import org.apache.hadoop.hbase.client.Result;
import org.apache.hadoop.hbase.client.ResultScanner;
import org.apache.hadoop.hbase.client.Scan;
import org.apache.hadoop.hbase.util.Bytes;

public class UserDao{
	public static final byte[] TABLE_NAME=Bytes.toBytes("user");	
	public static final byte[] INFP_FAM=Bytes.toBytes("info");
	
	public static final byte[] USER_COL=Bytes.toBytes("user");
	public static final byte[] NAME_COL=Bytes.toBytes("name");
	public static final byte[] EMAIL_COL=Bytes.toBytes("email");
	public static final byte[] PASS_COL=Bytes.toBytes("passwd");
	public static final byte[] TWEET_COL=Bytes.toBytes("tweet_count");
	
	
	private HConnection connection;
	
	public UserDao(HConnection connection){
		this.connection=connection;
	}
	
	private static Get mkGet(String user){
		
		//Get实例---用于读数据---创建Get实例的时候要使用行键来唯一标识---行键是第一重要的---就如同关系型数据库的主键一样
		Get g=new Get(Bytes.toBytes(user));
		//这里限定列族
		g.addFamily(INFP_FAM);
		return g;
	}
	
	private static Put mkPut(User u){
		//Put实例---用于写数据和修改数据---创建Put实例也要使用行键来唯一标识
		Put p=new Put(Bytes.toBytes(u.user));
		//这里在采用add方法给这行加入值,依次限定为列族,列限定名和单元值
		p.add(INFP_FAM,USER_COL,Bytes.toBytes(u.user));
		p.add(INFP_FAM,NAME_COL,Bytes.toBytes(u.name));
		p.add(INFP_FAM,EMAIL_COL,Bytes.toBytes(u.email));
		p.add(INFP_FAM,PASS_COL,Bytes.toBytes(u.passwd));
		p.add(INFP_FAM, TWEET_COL, Bytes.toBytes(u.tweet_count));
		return p;
	}
	
	private static Delete mkDel(String user){
		//Delete实例---用于删除数据---创建Delete实例也要使用行键来唯一标识
		
		//Scan实例---扫描实例
		
		//Increment实例
		Delete d=new Delete(Bytes.toBytes(user));
		return d;
	}
	
	private static Scan mkScan(){
		Scan scan=new Scan();
		scan.addFamily(UserDao.INFP_FAM);
		return scan;
	}
	
	public void addUser(String user,String name,String email,String passwd) throws IOException{
		HTableInterface users=connection.getTable(TABLE_NAME);
		Put p=mkPut(new User(user, name, email, passwd,0L));
		//提交实例
		users.put(p);
		users.close();
	}
	
	public User getUser(String user) throws IOException{
		HTableInterface users=connection.getTable(TABLE_NAME);
		Get g=mkGet(user);
		
		//提交实例
		Result result=users.get(g);
		if(result.isEmpty())
			return null;
		User u=new User(result);
		users.close();
		return u;
	}
	
	public void deleteUser(String user) throws IOException{
		HTableInterface users=connection.getTable(TABLE_NAME);
		Delete d=mkDel(user);
		users.delete(d);
		users.close();
	}
	
	public  List<User> getUsers() throws IOException{
		HTableInterface users=connection.getTable(TABLE_NAME);
		ResultScanner results = users.getScanner(mkScan());
		ArrayList<User> ret= new ArrayList<User>();
		for(Result r : results) 
			ret.add(new User(r));
		return ret;		
	}
	
	
}

