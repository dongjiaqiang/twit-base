package hbase.book.model;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import hbase.book.utils.MD5Utils;

import org.apache.hadoop.hbase.client.Delete;
import org.apache.hadoop.hbase.client.Get;
import org.apache.hadoop.hbase.client.HConnection;
import org.apache.hadoop.hbase.client.HTableInterface;
import org.apache.hadoop.hbase.client.Put;
import org.apache.hadoop.hbase.client.Result;
import org.apache.hadoop.hbase.client.ResultScanner;
import org.apache.hadoop.hbase.client.Scan;
import org.apache.hadoop.hbase.filter.CompareFilter.CompareOp;
import org.apache.hadoop.hbase.filter.Filter;
import org.apache.hadoop.hbase.filter.RegexStringComparator;
import org.apache.hadoop.hbase.filter.ValueFilter;
import org.apache.hadoop.hbase.util.Bytes;
import org.joda.time.DateTime;

public class TwitDao {

	public static final byte[] TABLE_NAME=Bytes.toBytes("twits");	
	public static final byte[] TWIT_FAM=Bytes.toBytes("twits");
	
	public static final byte[] USER_COL=Bytes.toBytes("user");
	public static final byte[] TWIT_COL=Bytes.toBytes("twit");

	
	private HConnection connection;
	
	public TwitDao(HConnection connection){
		this.connection=connection;
	}
	
	private static Put mkPut(Twit t){
		//Put实例---用于写数据和修改数据---创建Put实例也要使用行键来唯一标识
		int longLength=Long.SIZE/8;
		byte[] userHash=MD5Utils.md5sum(t.user);
		byte[] timeStamp=Bytes.toBytes(-1*t.time.getMillis());
		byte[] rowKey=new byte[MD5Utils.MD5_LENGTH+longLength];
		
		int offset=0;
		offset=Bytes.putBytes(rowKey, offset, userHash,0,userHash.length);
		Bytes.putBytes(rowKey, offset, timeStamp, 0, timeStamp.length);
		
		Put put=new Put(rowKey);

		put.add(TWIT_FAM,USER_COL,Bytes.toBytes(t.user));
		put.add(TWIT_FAM, TWIT_COL,Bytes.toBytes(t.twit));
		
		return put;
	}
	
	@SuppressWarnings("unused")
	private static Get mkGet(Twit t){
		
		int longLength=Long.SIZE/8;
		byte[] userHash=MD5Utils.md5sum(t.user);
		byte[] timeStamp=Bytes.toBytes(-1*t.time.getMillis());
		byte[] rowKey=new byte[MD5Utils.MD5_LENGTH+longLength];
		
		int offset=0;
		offset=Bytes.putBytes(rowKey, offset, userHash,0,userHash.length);
		Bytes.putBytes(rowKey, offset, timeStamp, 0, timeStamp.length);
		
		Get g=new Get(rowKey);
		g.addFamily(TwitDao.TWIT_FAM);
		return g;		
	}
	
	@SuppressWarnings("unused")
	private static Delete mkDelete(Twit t){
		
		int longLength=Long.SIZE/8;
		byte[] userHash=MD5Utils.md5sum(t.user);
		byte[] timeStamp=Bytes.toBytes(-1*t.time.getMillis());
		byte[] rowKey=new byte[MD5Utils.MD5_LENGTH+longLength];
		
		int offset=0;
		offset=Bytes.putBytes(rowKey, offset, userHash,0,userHash.length);
		Bytes.putBytes(rowKey, offset, timeStamp, 0, timeStamp.length);
		
		Delete d=new Delete(rowKey);
		d.deleteFamily(TwitDao.TWIT_FAM);
		return d;
	}
	
	private static Scan mkScan(){
		Scan scan=new Scan();
		scan.addFamily(TWIT_FAM);
		return scan;
	}
	
	private static Scan mkScan(String user){
		byte[] userHash=MD5Utils.md5sum(user);
		int longLength=Long.SIZE/8;
		byte[] startRow=Bytes.padTail(userHash, longLength);
		byte[] stopRow=Bytes.padTail(userHash, longLength);
		userHash[userHash.length-1]++;
		Scan s=new Scan(startRow, stopRow);
		return s;
	}
	
	private static Scan mkScan(String user,String include){
		Scan s;
		if(user.equals("all"))
			s=mkScan();
		else
			s=mkScan(user);
		s.addColumn(TWIT_FAM, TWIT_COL);
		Filter filter=new ValueFilter(CompareOp.EQUAL, new RegexStringComparator("*"+include+"*"));
		s.setFilter(filter);
		return s;
	}

	public void addTwit(String user,DateTime time,String twit) throws IOException{
		Put put=mkPut(new Twit(user, time, twit));
		HTableInterface twits=connection.getTable(TABLE_NAME);
		twits.put(put);
		twits.close();
		
		HTableInterface users=connection.getTable(UserDao.TABLE_NAME);
		users.incrementColumnValue(Bytes.toBytes(user),UserDao.INFP_FAM,UserDao.TWEET_COL,1L);
		users.close();		
	}
	
	
	
	//在HBase模式设计中行键设计至关重要---在HBase中,数据是按照行键排序的
	//HBase的行键设计是第一重要的考量,看到HBase时,我们必须问自己行键是什么,如何让行键更有效率
	
	public  List<Twit> getTwits() throws IOException{
		HTableInterface twits=connection.getTable(TABLE_NAME);
		ResultScanner results = twits.getScanner(mkScan());		
		ArrayList<Twit> ret= new ArrayList<Twit>();
		for(Result r : results) 
			ret.add(new Twit(r));
	    connection.close();
		return ret;		
	}
	
	public  List<Twit> getTwits(String user) throws IOException{
		HTableInterface twits=connection.getTable(TABLE_NAME);
		ResultScanner results = twits.getScanner(mkScan(user));
		ArrayList<Twit> ret= new ArrayList<Twit>();
		for(Result r : results) 
			ret.add(new Twit(r));
	    connection.close();
		return ret;		
	}
	
	public List<Twit> getTwits(String user,String include) throws IOException{
		HTableInterface twits=connection.getTable(TABLE_NAME);
		ResultScanner results = twits.getScanner(mkScan(user,include));
		ArrayList<Twit> ret= new ArrayList<Twit>();
		for(Result r : results) 
			ret.add(new Twit(r));
	    connection.close();
		return ret;		
	}		
}
