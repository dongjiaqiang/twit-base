package hbase.book.model;


import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.apache.hadoop.hbase.KeyValue;
import org.apache.hadoop.hbase.client.Result;
import org.apache.hadoop.hbase.util.Bytes;

public class Follow_1 {

	public String user;
	public List<String> follows;
	public long index=0L;
	public long count=0L;
	
	@Override
	public String toString() {
		// TODO Auto-generated method stub	
		return new String("<Follow: "+user+" "+StringUtils.join(follows, ",")+" "+count+">");
	}

	public Follow_1(String user, List<String> follows) {
		super();
		this.user = user;
		this.follows = follows;
	}
	
	@SuppressWarnings("deprecation")
	public Follow_1(Result r){
		this.user=Bytes.toString(r.getRow());
		this.count=Bytes.toLong(r.getValue(Follow_1Dao.FOLLOW_FAM, Follow_1Dao.COUNT_COL));
		this.index=Bytes.toLong(r.getValue(Follow_1Dao.FOLLOW_FAM, Follow_1Dao.INDEX_COL));
		List<KeyValue> follows=new ArrayList<KeyValue>();
		follows=r.list();
		this.follows=new ArrayList<String>();
		for(KeyValue kv:follows){
			String qualifier=Bytes.toString(kv.getQualifier());			
			if(!qualifier.equals(Bytes.toString(Follow_1Dao.COUNT_COL))&&!qualifier.equals(Bytes.toString(Follow_1Dao.INDEX_COL)))
				this.follows.add(Bytes.toString(kv.getValue()).trim());
		}
	}	
}
