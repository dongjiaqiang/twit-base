package hbase.book.model;

import org.apache.hadoop.hbase.client.Result;
import org.apache.hadoop.hbase.util.Bytes;


//构建一个简单的模型对象
public class User {

	public String user;
	public String name;
	public String email;
	public String passwd;
	public long tweet_count;
	
	@Override
	public String toString() {
		// TODO Auto-generated method stub
		return String.format("<User: %s, %s, %s, %d>", user,name,email,tweet_count);
	}
	
	public User(String user,String name,String email,String passwd,long tweet_count){
		this.user=user;
		this.name=name;
		this.email=email;
		this.passwd=passwd;
		this.tweet_count=tweet_count;
	}
	
	public User(Result result){
		this.user=Bytes.toString(result.getValue(UserDao.INFP_FAM, UserDao.USER_COL));
		this.name=Bytes.toString(result.getValue(UserDao.INFP_FAM, UserDao.NAME_COL));
		this.email=Bytes.toString(result.getValue(UserDao.INFP_FAM, UserDao.EMAIL_COL));
		this.passwd=Bytes.toString(result.getValue(UserDao.INFP_FAM, UserDao.PASS_COL));
		this.tweet_count=Bytes.toLong(result.getValue(UserDao.INFP_FAM, UserDao.TWEET_COL));
	}
}
