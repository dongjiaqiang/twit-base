package hbase.book;

import hbase.book.model.Twit;
import hbase.book.model.TwitDao;

import java.io.IOException;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.hbase.client.HConnection;
import org.apache.hadoop.hbase.client.HConnectionManager;
import org.joda.time.DateTime;

public class TwitsTool {

	public static final String usage="TwitsTool action ...\n"+
			 " help - print this message and exit.\n"+
            "  put twit user twit"+"- add a new twit.\n"+
			 " get user - retrieve a specific user .\n"+
            " list - list  twit.\n";	
	/**
	 * @param args
	 * @throws IOException 
	 */
	public static void main(String[] args) throws IOException {
		// TODO Auto-generated method stub
		Configuration conf=new Configuration();
			
		HConnection connection=HConnectionManager.createConnection(conf);
		
		TwitDao dao=new TwitDao(connection);
		
		if("put".equals(args[0])){
			System.out.println("put...");
			dao.addTwit(args[1],new DateTime(),args[2]);
			System.out.println("complete put...");
		}
		
		if("list".equals(args[0])&&args.length==1){
			System.out.println("list...");
			for(Twit t:dao.getTwits())
				System.out.println(t);
			System.out.println("complete list...");
		}
		
		if("list".equals(args[0])&&args.length==3){
			System.out.println("list "+args[1]+" "+args[2]+"...");
			for(Twit t:dao.getTwits(args[1], args[2]))
				System.out.println(t);
			System.out.println("complete list...");
		}		
		
	}

}
